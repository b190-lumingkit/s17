/* 
    FUNCTIONS:
        - lines/blocks of codes that tell our device/application to perform certain tasks when called/invoked

        - mostly created for complicated tasks to run several lines of code in succession

        - used to prevent repeating lines/blocks of codes that perform the same task/function
*/

//function declaration
/* 
    function - defines a function in javascript;
    printName() - functionName; functions are named to be able to use later in the code
    function block {} - the statements inside the curly brace which comprise of the body of the function. This is where the codes are to be executed

    syntax:
        function functionName(){
            function statements/code block;
        };
*/
function printName(){
    console.log("My name is John");
};

//call/invoke the function
/* 
    the code block and statements inside a function is not immediately executed when the function is defined/declared. The code block/statements inside a function is executed when the function is invoked/called.

    it is common to use the term "call a function" instead of "invoke a function"
*/
printName();

// DECLARATION vs EXPRESSION
// FUNCTION DECLARATION - using function keyword and functionName
declaredFunction();
/* 
    functions in javascript can be used before it can be defined, thus hoisting is allowed for javascript functions
        NOTE: hoisting is javascript's default behavior for certain variable and functions to run/use them before their declaration
*/
function declaredFunction(){
    console.log("This is a declared function");
};

// FUNCTION EXPRESSION
/* 
    a function can also be created by storing it inside a variable
        - does not allow hoisting

    a function expression si an anonymous function that is assigned to the variableFunction
        anonymous function - unnamed function
*/

let variableFunction = function(){
    console.log("This is function expression.");
};

variableFunction();

// mini-activity
function recommendedAnime(){
    console.log(
        "You should watch these anime: " +
        "\n1. Death Note" +
        "\n2. Jujutsu Kaisen" +
        "\n3. Boku no Hero Academia"
    );
}

function recommendedMovies(){
    console.log(
        "You should watch these movies: " +
        "\n1. Interstellar" +
        "\n2. Inception" +
        "\n3. Hangover"
    );
};

recommendedAnime();
recommendedMovies();

// end of mini-activity

// Reassigning declared functions
// we can also reassign declared functions and function expressions to new anonymous functions
declaredFunction = function(){
    console.log("Updated declaredFunction");
};

declaredFunction();

// However, we cannot change the declared functions/function expressions that are defined using const
const constFunction = function(){
    console.log("Initialized const function");
}
constFunction();

/* constFunction = function(){
    console.log("cannot be re-assigned");
};
constFunction(); */

// Function scoping
/* 
    scope is the accessibility of variables/functions

    JavaScript variables/functions have 3 scopes
        1. Local/Block scope - can be accessed inside the curly brace/code block {}
        2. Global Scope - outer most part of the codes (does not belong to any code block); can be accessed any code blocks
        3. 
*/
let globalVar = "Mr. Worldwide";
{
    let localVar = "Armando Perez";
    console.log(localVar);
    console.log(globalVar);
}
// console.log(localVar);

/* 
    function scope
        JS has also function scope: each function creates a new scope
        variables defined inside a function can only be accessed inside that function
*/

function showNames(){
    // the variables below can only be accessed inside the function showNames(). That is because they are defined inside the function
    var functionVar = "Joe";
    const functionConst = "John";
    let functionLet = "Jane";

    console.log(functionVar);
    console.log(functionConst);
    console.log(functionLet);
}

showNames();
/* console.log(functionVar);
console.log(functionConst);
console.log(functionLet); */

// Nested Functions
/* 
    functions that are defined inside another function. These nested functions have function scope where they can only be accessed inside the function where they are declared/defined.
*/
function newFunction(){
    let name = "Jane";

    function nestedFunction(){
        let nestedName = "John";
        console.log(nestedName);
    }
    console.log(name);
    nestedFunction();
};

newFunction();
// nestedFunction(); - returns an error because this function must be called inside the function where it is declared


// 2nd mini-activity
let globalVariable = "This is a global variable.";

function anotherFunction(){
    let nestedVariable = "This is a nested variable";
    console.log(nestedVariable);
    console.log(globalVariable);
}

anotherFunction();
// end of 2nd mini-activity

// Using alert()

/* alert("Hello World");

function showAlert(){
    alert("Hello Again!");
}

showAlert(); */

/* 
    NOTES ON USING alert()
        show only an alert() for short dialogs/messages to the user.
        do not overuse alert() because the program/js has to wait for it to be dismissed before continuing
*/

// using prompt()
    // prompt() - used to gather information from user; returns a string data type
/* let samplePrompt = prompt("Enter your name.")

console.log("Hello " + samplePrompt); */

/* let nullPrompt = prompt("Do not enter anything here");

console.log(nullPrompt); */

// 3rd mini-activity
/* function welcomeMessage(){
    let firstName = prompt("Hi! What's your first name?");
    let lastName = prompt("And your last name is?");

    console.log("Welcome " + firstName + " " + lastName + "!");
}

welcomeMessage(); */

// Function Naming Convention
    // Function names should be definitive of the task it will perform. It usually contains or starts with a verb/adjective

/* 
    1. Definitive of the task it will perform
    2. Name your functions with small letters; in cases of multiple words, use camelCasng (underscore is also valid)
*/
function getCourses(){
    let courses = ["Science 101", "Math 101", "English 101"];
    console.log(courses);
};
getCourses();

// avoid generic names to avoid confusion within our codes
function get(){
    let name = "Jamie";
    console.log(name);
};
get();

// avoid pointless and inappropriate name for our functions
function foo(){
    console.log(25%5);
};
foo();