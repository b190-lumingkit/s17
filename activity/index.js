/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function personalData(){
    let fullName = prompt("What's your full name?");
    let age = prompt("How old are you?");
    let location = prompt("Where are you located right now?");

    console.log("Hello, " + fullName);
    console.log("You are " + age + " years old.");
    console.log("You live in " + location);
};

personalData();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function favoriteBands(){
    console.log("My top 5 favorite bands are:")
    console.log(
        "1. Linkin Park" +
        "\n2. Panic at the Disco" +
        "\n3. Paramore" +
        "\n4. Parokya ni Edgar" +
        "\n5. December Avenue"
    );
}

favoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function favoriteMovies(){
    console.log("My top 5 favorite movies are:")
    console.log(
        "1. Interstellar" +
        "\nRotten Tomatoes Rating: 72%"
    );
    console.log(
        "2. Inception" +
        "\nRotten Tomatoes Rating: 87%"
    );
    console.log(
        "3. The Hangover" +
        "\nRotten Tomatoes Rating: 79%"
    );
    console.log(
        "4. The Dark Knight" +
        "\nRotten Tomatoes Rating: 94%"
    );
    console.log(
        "5. Avengers: End Game" +
        "\nRotten Tomatoes Rating: 94%"
    );
}

favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();


/* console.log(friend1);
console.log(friend2); */